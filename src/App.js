import React, {Component, Fragment} from 'react';
import {NavLink as RouterNavLink, Route, Switch} from 'react-router-dom';
import './App.css';
import Home from "./Containers/Home";
import Menu from "./Components/Menu/Menu";
import Orders from "./Components/Orders/Orders";
import EditDishes from "./Containers/EditDishes/EditDishes";
import NewDishes from "./Containers/NewDishes/NewDishes";
import {Collapse, Container, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";

class App extends Component {
  render() {
    return (
        <Fragment>
            <Navbar color="dark" light expand="md">
                <NavbarBrand color="light">Dishes</NavbarBrand>
                <NavbarToggler/>
                <Collapse isOpen navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/menu" color="light" exact>Dishes</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/add" color="light">New Dishes</NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
            <Container>
                <Switch>
                    <Route path="/" exact component={Home}></Route>
                    <Route path="/menu" component={Menu}></Route>
                    <Route path="/orders" component={Orders}></Route>
                    <Route path="/edit/:id" component={EditDishes}></Route>
                    <Route path="/add" exact component={NewDishes}/>
                    <Route render={() => <h1>Not Found !</h1>}/>
                </Switch>
            </Container>
        </Fragment>
    );
  }
}

export default App;
