import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import DishesForm from "../../Components/DishesForm/DishesForm";
import {addProduct} from "../../store/actions/actions";



class NewDishes extends Component {

    addProduct = product => {
        this.props.addProduct(product).then(() => {
            this.props.history.push('/');
        })

    };

    render() {
        return (
            <Fragment>
                <h1>Submit new dishes</h1>
                <DishesForm onSubmit={this.addProduct}/>
            </Fragment>

        );
    }
}

const mapDispatchToProps =(dispatch)=>({
    addProduct: product => dispatch(addProduct(product)),
});

export default connect (null,mapDispatchToProps) (NewDishes);