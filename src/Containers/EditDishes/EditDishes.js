import React, {Component, Fragment} from 'react';
import DishesForm from "../../Components/DishesForm/DishesForm";
import {addProduct} from "../../store/actions/actions";
import {connect} from 'react-redux';

class EditDishes extends Component {
    //
    // state={
    //     dishes: Object.keys(this.props.dishes).map((dish, id)=>{
    //         return({
    //             title:this.props.dishes[dish].title,
    //             img: this.props.dishes[dish].img,
    //             price:this.props.dishes[dish].price,
    //
    // })
    //     })
    // };



        
        // componentDidMount(){    };
        //     const  id = this.props.match.params.id;
        //     axios.get(`/quote/${id}.json`).then(response=>{
        //             console.log(response);
        //         this.setState({quotes: response.data})
        //     })


        // addProduct = product => {
        //      this.props.addProduct(product).then(() => {
        //         this.props.history.push('/');
        //     })
        //
        // };

    render() {
        let editbox = <DishesForm
             onSubmit={this.addProduct}
             dishes={this.state.dishes}
        /> ;
        return (
           <Fragment>
               <h1>Edit Dish</h1>
               {editbox}
           </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    dishes: state.dishes,
});

const mapDispatchToProps =(dispatch)=>({
    addProduct: product => dispatch(addProduct(product)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditDishes);
