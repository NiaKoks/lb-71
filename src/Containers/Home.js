import React, {Component} from 'react';
import Menu from "../Components/Menu/Menu";
import './Home.css'

class Home extends Component {
    render() {
        return (
            <div className='home-page'>
                <div className='menu-part'>
                    <Menu></Menu>
                </div>
            </div>
        );
    }
}

export default Home;