import axios from '../../axios-dishes';
import {ORDER_FAILURE, ORDER_REQUEST, ORDER_SUCCESS} from "./actions-types";

export const orderReq = () =>({type: ORDER_REQUEST});
export const orderSuc = dishes => ({type: ORDER_SUCCESS,dishes});
export const orderFail = error =>({type: ORDER_FAILURE});

export const ADD = 'ADD';
export const DEL = 'DEL';


export const addProduct = product =>{
    return dispatch =>{
        dispatch(orderReq());
        return axios.post('dishes.json', product) }};

export const takeDishes= product =>{
    return dispatch =>{
        dispatch(orderReq());
        axios.get('dishes.json').then(responce =>{
                dispatch(orderSuc(responce.data))
            },error => dispatch(orderFail(error))
        )
    }
};

export const deleteOrder = (orderData) => {
    return dispatch => {
        dispatch(orderReq());
        axios.delete('dishes/' + orderData + '.json').then(
            response => {
                dispatch(takeDishes());
            },
            error => dispatch(orderFail(error))
        );
    }
};


export const  editDish = product =>{
    const  id = this.props.match.params.id;
    axios.put(`/dishes/${id}.json`, product);
  };



