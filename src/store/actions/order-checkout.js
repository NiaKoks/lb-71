import {ADD, DEL, INITDISH} from "./actions-types";

export const addToCart = (name,price) =>({type: ADD,name,price});
export const delFromCart = (name,price)=>({type:DEL,name,price});
export const initDishes = () => ({type:INITDISH});