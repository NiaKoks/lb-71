import {
    ORDER_REQUEST,
    ORDER_SUCCESS,
    ORDER_FAILURE,
} from '../actions/actions-types'

const initialState ={
    dishes: {},
    loading: false,
    error: null,
};

const addDishReducer = (state=initialState,action) =>{
    switch (action.type) {
        case ORDER_REQUEST: {
            return{
                ...state,
                loading: true,
            }
        };
        case ORDER_SUCCESS: {
            return{
                ...state,
                loading:false,
                dishes: action.dishes,
            }
        };
        case ORDER_FAILURE: {
            return{
                ...state,
                loading:false,
                error: action.error
            }
        };
        default:
            return state;
    }
};

export default addDishReducer;