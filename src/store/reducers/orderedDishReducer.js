import {
    ADD,
    DEL,
    INITDISH,
} from '../actions/actions-types'


const initialState ={
    dishes: {},
    totalPrice:0,
    delivery: 150,
};
const orderedDishesReducer =(state=initialState,action)=>{
    switch (action.type) {
        case ADD:{
            return{
                ...state,
                dishes: {
                    ...state.dishes,
                    [action.name]: state.dishes[action.name] + 1,
                },
                totalPrice: state.totalPrice + action.price,
            }
        };
        case DEL:{
            if (state.dishes[action.name] >0){
                return{
                    ...state,
                    dishes: {
                        ...state.dishes,
                        [action.name]: state.dishes[action.name] -1,
                    },
                    totalPrice: state.totalPrice - action.price,
                }
            } else return state

        };
        case INITDISH:{
            return{
                ...state,
                dishes: {...initialState},
                totalPrice: state.totalPrice,
            }
        }

        default:
            return state;
    }
}

export default orderedDishesReducer;