import React, {Component,Fragment} from 'react';
import {connect} from 'react-redux';
import {deleteOrder, takeDishes} from "../../store/actions/actions";
import './Menu.css';
class Menu extends Component {
    componentDidMount() {
        this.props.takeDishes();
    };

    editHandler = (id) => {
        this.props.history.push(`/edit/${id}`);
    };

    render() {
        return (
            <div>
                <Fragment>
                    {Object.keys(this.props.dishes).map((dish, index) => {
                        return (
                            <div key={index} className='ProductCard'>
                                <h3>{this.props.dishes[dish].title}</h3>
                                <button onClick={() => this.Handler(dish)} className='del-edit-btn'>Edit</button>
                                <button className='del-edit-btn'  onClick={()=> this.props.deleteOrder(dish)}>X</button>
                                <img className='ProductImg' src={this.props.dishes[dish].img} alt=""/>
                                <p>{this.props.dishes[dish].price} KGS</p>
                            </div>
                        )
                    })}
                </Fragment>
            </div>
        );
    }
}
const mapStateToProps =(state)=>({
    dishes: state.menu.dishes || null,
    dishesForCart: state.make.dishes,
});
const mapDispatchToProps =(dispatch)=>({
    takeDishes: () => dispatch(takeDishes()),
    deleteOrder: dish => dispatch(deleteOrder(dish)),

});
export default connect(mapStateToProps, mapDispatchToProps) (Menu);