import React, {Component} from 'react';
import {NavLink} from "react-router-dom";
import './Nav.css'
class Nav extends Component {
    render() {
        return (
            <div className='nav-link'>
                <NavLink to="/menu">Dishes</NavLink>
                <NavLink to="/orders">Orders</NavLink>
            </div>
        );
    }
}

export default Nav;