import React, {Component} from 'react';
import './DishesForm.css';
import {Button, Col, Form, FormGroup, Input, Label} from 'reactstrap';


class DishesForm extends Component {
    constructor(props){
        super(props);
        if(props.dishes){
            this.state = {...props.dishes}
        }else{
            this.state= {
                title:'',
                price: 0,
                img: ''
            }
        }
    }

    valueChanged =(event) =>{
        const{name , value} = event.target;
        this.setState({[name]:value})
    };

    submitHandler = event=>{
        event.preventDefault();
       this.props.onSubmit({...this.state})
    };

    render() {
        return (
            <Form  onSubmit={this.submitHandler}>
                <FormGroup row>
                    <Label for="title" sm={2}>Dishes name:</Label>
                    <Col sm={10}>
                        <Input type="text" name="title"  id="title"
                        value={this.state.title} onChange={this.valueChanged}
                         />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="price" sm={2}>Price:</Label>
                    <Col sm={10}>
                        <Input type="text" name="price"  id="price"
                               value={this.state.price} onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="img" sm={2}>Image:</Label>
                    <Col sm={10}>
                        <Input type="text" name="img"  id="img"
                               value={this.state.img} onChange={this.valueChanged}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col sm={{size:10 , offset: 2}}>
                    <Button type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default DishesForm;