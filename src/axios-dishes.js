import axios from 'axios';

const instance =axios.create({
    baseURL: 'https://lb-71-nia.firebaseio.com/'
});

export default instance;